---
openapi: 3.0.1
info:
  title: Teamride
  version: 2.0.0
  description: API dokumentácia pre projekt Teamride
  contact: {}
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0
servers:
- url: https://teamride.eu/
paths:
  /users:
    description: Endpoint pre zoznam userov
    get:
      tags:
      - User
      responses:
        200:
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/User'
              examples:
                getUsers:
                  value: "{\r\n  \"Users\": [{\r\n    \"ID\": 1,\r\n    \"Name\":\
                    \ \"Psohlavec\",\r\n    \"City\": \"Trnava\",\r\n    \"Phone\"\
                    : \"+911987654\",\r\n    \"Car\": {\r\n      \"ID\": 1,\r\n  \
                    \    \"Brand\": \"Skoda\",\r\n      \"SPZ\": \"BA123XX\",\r\n\
                    \      \"Color\": \"fialova\"\r\n    }\r\n  }, {\r\n    \"ID\"\
                    : 2,\r\n    \"Name\": \"PPP\",\r\n    \"City\": \"Nitra\",\r\n\
                    \    \"Phone\": \"456\",\r\n    \"Car\": {\r\n      \"ID\": 2,\r\
                    \n      \"Brand\": \"Skoda\",\r\n      \"SPZ\": \"KK234II\",\r\
                    \n      \"Color\": \"cierna\"\r\n    }\r\n  }"
          description: Successful response - returns an array of `User` entities.
        401:
          description: Je potreben sa prihlasit
      security:
      - application:
        - read
      operationId: getUsers
      summary: List All Users
      description: Gets a list of all `User` entities.
    post:
      requestBody:
        description: A new `User` to be created.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
            examples:
              user:
                value:
                  City: some text
                  Name: some text
                  Email: some text
                  Phone: some text
        required: true
      tags:
      - User
      responses:
        201:
          content:
            application/json:
              schema:
                type: string
              examples:
                userCreated:
                  value: '"User created with name: Psohlavec"'
          description: Successful response.
        400:
          description: Chyba v spracovani JSON
        401:
          description: Je potreben sa prihlasit
        422:
          description: Chyba v nacitani do schemy - deserializacia
      security:
      - application:
        - write
      operationId: createUser
      summary: Create a User
      description: Creates a new instance of a `User`.
  /users/{userId}:
    description: Endpoint pre detail konkretneho usera
    get:
      tags:
      - getUser
      parameters:
      - name: userId
        description: A unique identifier for a `User`.
        schema:
          type: string
        in: path
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
              examples:
                getUser:
                  value:
                    ID: 2
                    Name: PPP
                    City: Nitra
                    Phone: "456"
                    Car:
                      ID: 2
                      Brand: Skoda
                      SPZ: KK234II
                      Color: cierna
          description: Successful response - returns a single `User`.
        401:
          description: Je potreben sa prihlasit
        404:
          description: |
            User not found
      security:
      - application:
        - read
      operationId: getUser
      summary: Get a User
      description: Gets the details of a single instance of a `User`.
    delete:
      tags:
      - User
      parameters:
      - name: userId
        description: A unique identifier for a `User`.
        schema:
          type: string
        in: path
        required: true
      responses:
        200:
          description: |
            Succesful delete
        401:
          description: Je potreben sa prihlasit
        404:
          description: |
            User not found
      security:
      - application:
        - write
      operationId: deleteUser
      summary: Delete a User
      description: Deletes an existing `User`.
    patch:
      requestBody:
        description: Updated `User` information.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
            examples:
              patchUser:
                value:
                  City: some text
                  Name: some text
                  Email: some text
                  Phone: some text
        required: true
      tags:
      - User
      parameters:
      - name: userId
        description: A unique identifier for a `User`.
        schema:
          type: string
        in: path
        required: true
      responses:
        200:
          description: Succesful patch
        400:
          description: Chyba v spracovani JSON
        401:
          description: Je potreben sa prihlasit
        404:
          description: User not found
        422:
          description: Chyba v nacitani do schemy - deserializacia
      security:
      - application:
        - write
        - read
      operationId: updateUser
      summary: Update a User
      description: Updates an existing `User`.
  /cars:
    description: Endpoint pre ziskanie zoznamu aut a vytvaranie aut
    get:
      tags:
      - getCars
      responses:
        200:
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Car'
              examples:
                example:
                  value:
                    Cars:
                    - ID: 1
                      Brand: Skoda
                      SPZ: BA123XX
                      Color: fialova
                    - ID: 2
                      Brand: Skoda
                      SPZ: KK234II
                      Color: cierna
          description: Successful response - returns an array of `Car` entities.
        401:
          description: Je potrebne sa prihlasit
      security:
      - application:
        - read
      operationId: getCars
      summary: List All Cars
      description: Gets a list of all `Car` entities.
    post:
      requestBody:
        description: A new `Car` to be created.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Car'
            examples:
              CreateCar:
                value:
                  ID: 1
                  Brand: Skoda
                  SPZ: BA123XX
                  Color: fialova
        required: true
      tags:
      - createCar
      responses:
        201:
          description: Successful response.
        400:
          description: Chyba v nacitani JSON
        401:
          description: Je potrebne sa prihlasit
        409:
          description: Car existing - kontrola so SPZ
        422:
          description: Chyba v nacitani do schemy - deserializacia
      security:
      - application:
        - write
      operationId: createCar
      summary: Create a Car
      description: Creates a new instance of a `Car` - automaticky sa prideli prihlasenemu
        userovi
  /rides:
    description: Endpoint pre zoznam vsetkych jazd a vytvorenie novej jazdy
    get:
      tags:
      - Ride
      parameters:
      - name: date
        description: 'Format: 2019-06-15T17:20:00+00:00'
        schema:
          type: string
        in: query
        required: true
      - name: gps_from
        description: 'Format: 17.06005079780406%2C%2048.17584750907156'
        schema:
          type: string
        in: query
        required: true
      - name: gps_to
        description: 'Format: 17.06005079780406%2C%2048.17584750907156'
        schema:
          type: string
        in: query
        required: true
      - name: from
        description: 'Format: Jaguar Landrover, Nitra'
        schema:
          type: string
        in: query
      responses:
        200:
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Ride'
              example:
                ID: 1
                From:
                  Longitude: "17.2342"
                  Latitude: "18.31231"
                To:
                  Longitude: "17.2342"
                  Latitude: "18.31231"
                IsReturn: true
                StartRide: 12:00
                ArrivalTime: 12:00
                RepeatDays: []
                Routes:
                - route1:
                  - point1:
                      Longitude: "17.2342"
                      Latitude: "18.31231"
                    point2:
                      Longitude: "17.2342"
                      Latitude: "18.31231"
                  route2:
                  - point1:
                      Longitude: "17.2342"
                      Latitude: "18.31231"
                    point2:
                      Longitude: "17.2342"
                      Latitude: "18.31231"
                CarID: 1
                IsDirect: true
                RideWithUserIDs: []
          description: Successful response - returns an array of `Ride` entities.
        400:
          content:
            application/json:
              schema:
                type: string
              examples:
                badDate:
                  value: Missing query param date
                badGpsFrom:
                  value: Missing query param gps from
                badGpsTo:
                  value: Missing query param gps to
                badRequest:
                  value: bad query param
          description: Bad parameters
        401:
          description: Je potrebne sa prihlasit.
        404:
          content:
            application/json:
              schema:
                type: string
              examples:
                notFound:
                  value: Rides not found!
          description: Rides not found!
      security:
      - application:
        - write
      operationId: getRides
      summary: List All Rides
      description: Gets a list of all `Ride` entities.
    post:
      requestBody:
        description: A new `Ride` to be created.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Ride'
            examples:
              rides:
                value:
                  From: Trnava
                  FromLon: "17.600510"
                  FromLat: "48.387977"
                  To: Nitra
                  ToLon: "18.049032"
                  ToLat: "48.346011"
                  Date:
                  - 2019-06-15T00:00:00+00:00
                  - 2019-06-16T00:00:00+00:00
                  - 2019-06-17T00:00:00+00:00
                  Time: 2019-06-15T06:50:00+00:00
                  TimeETA: 2019-06-15T07:50:00+00:00
                  Note: Tesco
                  User_ID: 5
                  Car_ID: 4
                  NumberOfPlaces: 3
                  IsReturn: true
                  ReturnTime: 2019-06-15T16:50:00+00:00
                  ReturnTimeETA: 2019-06-15T17:50:00+00:00
                  ReturnNote: JLR
                  ReturnNumberOfPlaces: 2
        required: true
      tags:
      - Ride
      responses:
        201:
          description: Successful response.
        400:
          content:
            application/json:
              schema:
                type: string
          description: |
            Chyba nacitania JSON
        401:
          description: Je potreben sa prihlasit
        422:
          description: chyba nacitania do schemy - deserializacia
      security:
      - application:
        - write
      operationId: createRide
      summary: Create a Ride
      description: Creates a new instance of a `Ride`.
  /cars/{carId}:
    description: 'Endpoint pre detail konkretneho auta '
    get:
      tags:
      - getCar
      parameters:
      - name: carId
        description: A unique identifier for a `Car`.
        schema:
          type: integer
        in: path
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Car'
              examples:
                getC:
                  value:
                    ID: 18
                    SPZ: some text
                    Color: some text
                    Brand: some text
          description: Successful response - returns a single `Car`.
        401:
          description: Potrebne sa prihlasit
        404:
          content:
            application/json:
              schema:
                type: string
              examples:
                notFound:
                  value: '"Car not found for id: 1"'
          description: Car not found
      security:
      - application:
        - read
      operationId: getCar
      summary: Get a Car
      description: Gets the details of a single instance of a `Car`.
    delete:
      tags:
      - deleteCar
      parameters:
      - name: carId
        description: A unique identifier for a `Car`.
        schema:
          type: integer
        in: path
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                type: string
              examples:
                deleteCar:
                  value: '"Car with id: 1 deleted"'
          description: |
            Succesful response
        401:
          description: Je potrebne sa prihlasit
        404:
          content:
            application/json:
              schema:
                type: string
              examples:
                notFound:
                  value: '"Car not found for id: 1"'
          description: |
            Car not found
      security:
      - application:
        - write
      operationId: deleteCar
      summary: Delete a Car
      description: Deletes an existing `Car`.
    patch:
      requestBody:
        description: Updated `Car` information.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Car'
            examples:
              patchCar:
                value:
                  ID: 72
                  SPZ: some text
                  Color: some text
                  Brand: some text
              patchCarParameter:
                value:
                  SPZ: some text
        required: true
      tags:
      - updateCar
      parameters:
      - name: carId
        description: A unique identifier for a `Car`.
        schema:
          type: integer
        in: path
        required: true
      responses:
        200:
          description: Succesful patch
        400:
          description: Chyba v nacitani JSON
        401:
          description: Je potreben sa prihlasit
        404:
          description: Car not found
        422:
          description: Chyba v nacitani do schemy - deserializacia
      security:
      - application:
        - write
        - read
      operationId: updateCar
      summary: Update a Car
      description: Updates an existing `Car`.
    parameters:
    - name: carId
      in: path
      required: true
  /rides/{rideId}:
    description: Endpoint pre konkretnu jazdu
    get:
      tags:
      - Ride
      parameters:
      - name: rideId
        description: A unique identifier for a `Ride`.
        schema:
          type: number
        in: path
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Ride'
              examples:
                ride:
                  value:
                    ID: 74
                    From: Polianky 5, Bratislava
                    To: Jaguar Landrover, Nitra
                    StartDate: 2019-06-21T07:39:04+00:00
                    EndDate: 2019-06-21T08:41:45+00:00
                    Note: Fast fast sad fast fsad
                    NumberOfPlaces: 3
                    User:
                      ID: 16
                      Name: Tester, Testovicov
                      Phone: "0910889716"
                      Car:
                        ID: 10
                        Brand: Skoda
                        SPZ: BAxxxx
                        Color: Modra
                    RideWithUsersDetail:
                    - ID: 16
                      Name: Tester, Testovicov
                      City: Továrenská 21, Bratislava
                      Phone: "0910889716"
                      Email: Jako@hrasko333.sk
                    Lat: "48.1758475090716"
                    Lon: "17.0600507978041"
          description: Successful response - returns a single `Ride`.
        401:
          description: Je potreben sa prihlasit
        404:
          content:
            application/json:
              schema:
                type: string
              examples:
                getRide:
                  value: '"Ride not found for id"'
          description: Not Found
      security:
      - application:
        - read
        - write
      operationId: getRide
      summary: Get a Ride
      description: Gets the details of a single instance of a `Ride`.
    delete:
      tags:
      - Ride
      parameters:
      - name: rideId
        description: A unique identifier for a `Ride`.
        schema:
          type: number
        in: path
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                type: string
              examples:
                deleted:
                  value: '"Ride with id: 1 deleted"'
          description: |
            Ride deleted
        401:
          description: Je potreben sa prihlasit
        404:
          content:
            application/json:
              schema:
                type: string
              examples:
                notFound:
                  value: '"Ride not found for id: 1"'
          description: Ride not found
      security:
      - application:
        - write
      operationId: deleteRide
      summary: Delete a Ride
      description: Deletes an existing `Ride`.
  /token/:
    description: Endpoint pre prihlasenie/odhlasenie - ziskanie tokenov
    get:
      tags:
      - Login
      parameters:
      - name: refresh_token
        description: JWT refresh_token
        schema:
          type: string
        in: query
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                type: string
              examples:
                token:
                  value:
                    acces_token_expires_in: 2019-07-09T12:12:52+00:00
                    refresh_token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1NzMwMzUxNzIsInN1YiI6IjQ1In0.0EHZws6FGSET6KwwTSHbzAMSseyAOouKIcp-u6uJsOo1tQN1wUpDukVNB2rmiXJKthLxcLXU7QAP0hz0Ozriug
                    refresh_token_expires_in: 2019-11-06T10:12:52+00:00
                    acces_token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NjI2NzQzNzIsInN1YiI6IjQ1In0.wmwW9bxm8JCffrKwq9JWVVDXUF9kze3PEcPdi47_XDE
          description: Response ok
        400:
          description: Refresh_token is missing in query param
        403:
          content:
            application/json:
              schema:
                type: string
          description: |-
            Refresh token expired

            Je potrebne sa prihlasit emailom a heslom
      security:
      - {}
      operationId: updateAccesToken
      summary: Update acces token with refresh token
    post:
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Notification'
            examples:
              push:
                value:
                  PushID: test
                  OsType: iOS
                  Sandbox: true
        required: true
      tags:
      - login
      parameters:
      - name: Basic
        description: email:heslo zasifrovane pomocou base64
        schema:
          type: string
        in: header
        required: false
      responses:
        200:
          content:
            application/json:
              schema:
                type: string
              examples:
                token:
                  value:
                    acces_token_expires_in: 2019-07-09T12:12:52+00:00
                    refresh_token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1NzMwMzUxNzIsInN1YiI6IjQ1In0.0EHZws6FGSET6KwwTSHbzAMSseyAOouKIcp-u6uJsOo1tQN1wUpDukVNB2rmiXJKthLxcLXU7QAP0hz0Ozriug
                    refresh_token_expires_in: 2019-11-06T10:12:52+00:00
                    acces_token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NjI2NzQzNzIsInN1YiI6IjQ1In0.wmwW9bxm8JCffrKwq9JWVVDXUF9kze3PEcPdi47_XDE
          description: |
            Vygenerovanie tokenov
        400:
          content:
            application/json:
              schema:
                type: string
              examples:
                Authorization:
                  value: '"Missing Authorization"'
                BadJson:
                  value: '"Bad JSON"'
          description: |
            Authorization is missing or != Basic or Bad Json
        403:
          description: Email or Password doesnt match
      security:
      - {}
      operationId: login
      summary: login with email and password
    delete:
      tags:
      - login
      parameters:
      - name: push_id
        description: '"string"'
        schema:
          type: string
        in: query
        required: true
      responses:
        200:
          description: |-
            uspesny logout
            vymazanie pushID z tabulky
        400:
          description: Bad JSON with pushID
        404:
          description: |
            User or PushID not found
      security:
      - application:
        - write
        - read
      operationId: logout
      description: logout
  /me/:
    description: Endpoint pre moj profil, zobrazenie a zmena profilu
    get:
      tags:
      - UserProfile
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
              examples:
                getProfile:
                  value:
                    ID: 8
                    City: some text
                    Name: some text
                    Password: '**********'
                    Email: some text
                    Phone: some text
                    CDSID: some text
                    Car:
                      ID: 21
                      SPZ: some text
                      Color: some text
                      Brand: some text
                    FromLon: lon
                    FromLat: lat
          description: Successful response - return a my profile
        401:
          description: Je potrebne sa prihlasit
      security:
      - application:
        - read
      operationId: getLogedUser
      summary: Get logged user
      description: Get user profile
    patch:
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
            examples:
              patchUser:
                value:
                  City: some text
                  Name: some text
                  Email: some text
                  Phone: some text
        required: true
      tags:
      - UserProfile
      responses:
        200:
          description: Succesful patch
        400:
          description: Chyba pri spracovani JSON
        401:
          description: Je potreben sa prihlasit
        404:
          description: Not found
        422:
          description: Chyba pri nacitani do schemy - deserializacia
      security:
      - application:
        - write
        - read
      operationId: updateLoggedUser
      summary: Update logged User
      description: Updates an existing `User`.
  /me/rides:
    description: Endpoint pre zobrazenie zoznamu mojich jazd
    get:
      tags:
      - Ride
      - UserProfile
      parameters:
      - name: date
        description: 'Format: 2019-06-15T17:20:00+00:00'
        schema:
          type: string
        in: query
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Ride'
          description: Successful response - returns a my Rides `Ride`.
        400:
          content:
            application/json:
              schema:
                type: string
              examples:
                badParamDate:
                  value: Missing query param date
          description: Bad parameters
        401:
          description: Je potreben sa prihlasit
        404:
          content:
            application/json:
              schema:
                type: string
              examples:
                notFound:
                  value: '"Rides not found"'
          description: Rides not found
      security:
      - application:
        - read
      operationId: getMyRides
      summary: List All my `Ride`
  /users/{userId}/rides:
    summary: endpoint for all rides of specific user
    description: Zobrazenie zoznamu jazd konkretneho usera
    get:
      tags:
      - getRides
      responses:
        200:
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Ride'
          description: 'List of all rides '
        401:
          description: Je potrebne sa prihlasit
        404:
          description: Rides not found
      security:
      - application:
        - read
      operationId: getRidesUser
      summary: get of all rides from specific user
    parameters:
    - name: userId
      in: path
      required: true
  /rides/{rideId}/passenger/{userId}:
    description: Endpoint pre rezervovanie a odhlasenie miesta v aute
    put:
      tags:
      - Reservation
      parameters:
      - name: userId
        in: path
        required: true
      - name: rideId
        in: path
        required: true
      responses:
        201:
          content:
            application/json:
              schema:
                type: string
              examples:
                '&created':
                  value: '"Reservation created"'
          description: |
            Rezervacia vytvorena
        401:
          description: Je potrebne sa prihlasit
        409:
          content:
            application/json:
              schema:
                type: string
              examples:
                RideFull:
                  value: '"Ride is full"'
                ReservationExist:
                  value: '"Reservation already exist"'
          description: Rides is full or Reservation is existing
        422:
          description: |
            Chyba v nacitani rezervacie
      security:
      - application:
        - write
        - read
      operationId: createReservation
      description: Join to specific Ride with specific User
    delete:
      tags:
      - Reservation
      parameters:
      - name: rideId
        in: path
        required: true
      - name: userId
        in: path
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                type: string
              examples:
                Deleted:
                  value: '"Reservation cancelled"'
          description: |
            Rezervacia zrusena
        401:
          description: Je potrebne sa prihlasit
        404:
          description: not Found
      security:
      - application:
        - write
        - read
      operationId: cancelReservation
      summary: Cancel reservation
      description: 'Cancel reservation in ride '
components:
  schemas:
    User:
      title: Root Type for User
      description: The root of the User type's schema.
      required: []
      type: object
      properties:
        ID:
          type: integer
        City:
          type: string
        Name:
          type: string
        Password:
          format: password
          type: string
        Email:
          type: string
        Phone:
          type: string
        CDSID:
          description: Unique indentificator
          type: string
        Car:
          $ref: '#/components/schemas/Car'
          description: NestedSchema of Car
      example: |-
        {
            "ID": 1,
            "City": "Dolnozemská 1, Bratislava",
            "Name": "Janko Hrasko",
            "Email": "loginName",
            "Password": "password",
            "PhoneNumber": "+421901321123",
            "CDSID": "hraskoj1"
            "Car": {
                "Brand": "Skoda",
                "Color": "zelena",
                "SPZ": "NM123XX"
            },
            "FromLon": "17.600510",
            "FromLat": "48.387977"
        }
    Car:
      title: Root Type for Car
      description: The root of the Car type's schema.
      type: object
      properties:
        ID:
          format: int32
          type: integer
        SPZ:
          type: string
        Color:
          type: string
        Brand:
          type: string
      example: |-
        {
            "ID": 1,
            "SPZ": "NR123XX",
            "Brand": "Skoda Octavia",
            "Color": "zelena",
            "NumberOfPlaces": 4,
        }
    Ride:
      title: Root Type for Ride
      description: The root of the Ride type's schema.
      required: []
      type: object
      properties:
        ID:
          type: integer
        From:
          description: Odkial
          type: string
          properties:
            Latitude:
              type: string
            Longitude:
              type: string
        To:
          description: |
            Kam
          type: string
          properties:
            Latitude:
              type: string
            Longitude:
              type: string
        CarID:
          type: integer
        StartDate:
          format: date-time
          description: 'Format: 2019-06-21T07:39:04+00:00'
          type: string
        EndDate:
          format: date-time
          description: 'Format: 2019-06-21T07:39:04+00:00'
          type: string
        Note:
          description: ""
          type: string
        NumberOfPlaces:
          description: Pocet miest v aute
          type: integer
        User:
          $ref: '#/components/schemas/User'
          description: Detail of User
        RideWithUsers:
          description: Number of Users joined in Ride
          type: integer
        RideWithUsersDetail:
          $ref: '#/components/schemas/User'
          description: Detail info of Users joined in Ride
        Driver:
          description: |-
            Driver in Ride = True
            Passenger in Ride = False
          type: boolean
        Distance:
          format: float
          description: Distance from my point to searched point
          type: number
        Walking:
          format: float
          description: Time of walking to pickup point
          type: number
        FromLon:
          description: Longtitude of From
          type: string
        FromLat:
          description: Latitude of From
          type: string
        ToLon:
          description: Longtitude of To
          type: string
        ToLat:
          description: Latitude of To
          type: string
      example:
        ID: 74
        From: Polianky 5, Bratislava
        To: Jaguar Landrover, Nitra
        StartDate: 2019-06-21T07:39:04+00:00
        EndDate: 2019-06-21T08:41:45+00:00
        Note: Fast fast sad fast fsad
        NumberOfPlaces: 3
        User:
          ID: 16
          Name: Tester, Testovicov
          Phone: "0910889716"
          Car:
            ID: 10
            Brand: Skoda
            SPZ: BAxxxx
            Color: Modra
        RideWithUsersDetail:
        - ID: 16
          Name: Tester, Testovicov
          City: Továrenská 21, Bratislava
          Phone: "0910889716"
          Email: Jako@hrasko333.sk
    Notification:
      title: Root Type for Notification
      description: ""
      type: object
      properties:
        PushID:
          type: string
        OsType:
          type: string
        Sandbox:
          type: boolean
      example:
        PushID: test
        OsType: iOS
        Sandbox: true
  securitySchemes:
    application:
      flows:
        clientCredentials:
          tokenUrl: http://example.com/oauth/token
          scopes:
            write: allows modifying resources
            read: allows reading resources
      type: oauth2
      description: 'vo vsetkych endpointoch okrem registracie - POST na /rides a endpointu
        TOKEN je potrebne v hlavicke Authorization pouzivat string: Bearer acces_token'
security:
- application:
  - write
  - read
