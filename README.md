# TeamRide

Backend strana pre projekt TeamRide - zdielana doprava do prace

## Tools

OS Linux(Mint 19.01)
Programovaci jazyk - Python (framework: Falcon)

Local host server - Gunicorn
Reverse Proxy - Nginx (+SSL)

Serializacia/Deserializacia dat do JSON - Marshmallow 
Autentifikacia: JWT (JavaWebToken)

DB - Postgre + PostGis
ORM - SQLAlchemy, GEOAlchemy2
AWS (Hosting od Amazonu) - EC2, RDS, Route 53, 

## Setup 

```
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

## Start app local

```
gunicorn -b localhost:5000 'app:create_app()' --reload
```

## Example: GET profile

```
curl localhost:5000/me
```

## Setup Nginx - reverse proxy

Instalacia nginx a vytvorenie konfiguracneho suboru

```
apt-get install nginx -y
sudo vim /etc/nginx/sites-available/app.conf
```

Nastavenie serveru a proxyPass

```
server {
listen 80 
server_name dev.teamride.eu 

location / {
include proxy_params 
proxy_pass http://localhost:5000 
}
}
```

Vytvorenie simlinku, odstranenie default nastavenia, restart nginx a povolenie portu 80

```
ln -s /etc/nginx/sites-available/app.conf /etc/nginx/sites-enabled/app.conf
rm etc/nginx/sites-enabled/default
nginx -t
systemctl restart nginx
ufw allow 80
```

## Setup supervisor

Vytvorim si konfiguracny subor:

```
sudo vim /etc/supervisor/conf.d/app.conf
```

Doplnim priecinok, spustaci prikaz, autorestart a nastavenie logov a errorov

```
[program:app]
command=/home/ubuntu/carpool/.venv/bin/gunicorn 'app:create_app()' -b localhost:5000 --reload
directory=/home/ubuntu/carpool/carpool_server/src
autostart=true
autorestart=true
stderr_logfile=/var/log/app.err.log
stdout_logfile=/var/log/app.out.log
```
spustenie service a restart 


```
sudo supervisorctl start app
sudo supervisorctl reread
sudo service supervisor restart
sudo supervisorctl status
```

## PostgreSQL setup

* CREATE DATABASE carpool;
* GRANT ALL PRIVILEGES ON DATABASE carpool TO test;
* CREATE EXTENSION postgis;
* CREATE EXTENSION pgcrypto;

