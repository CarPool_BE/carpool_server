from sqlalchemy.orm import sessionmaker
from models import Base
import models


def db_init(engine):
	#Base.metadata.drop_all(engine)
	Base.metadata.create_all(engine)

	#Session = sessionmaker(bind=engine)
	#session = Session()
	#r1 = models.Ride(from_='Trnava', to='Nitra', car_id=1, user_id=1,gps='POINT(17.5362129975589 48.3561789948762)', places=3)
	#r2 = models.Ride(from_ ='Bratislava', to='Nitra', car_id=2, user_id=2, places=2, gps='POINT(17.0239322976302 48.1530706948841)', ride_with_users=[3,4])
	#a1 = models.RideUsers(user_id='3', ride_id='1')
	#a2 = models.RideUsers(user_id='5', ride_id='1')
	#session.add(a1)
	#session.add(a2)

	#session.commit()

	#session.close()
'''
	c1 = models.Car(brand='Skoda', model='Octavia', spz='BA123XX', color='zelena', places=4, fuel=1)
	c2 = models.Car(brand='Skoda', model='Fabia', spz='NR123XX', color='cierna', places=2, fuel=1)
	c3 = models.Car(brand='Volkswagen', model='Golf', spz='TT123XX', color='modra', places=3, fuel=2)
	u1 = models.User(name='Psohlavec', city='Trnava', phone='123', car_id='1')
	u2 = models.User(name='Test', city='Nitra', phone='456', car_id='2')
	u3 = models.User(name='Izidor', city='Bratislava', phone='789')

	session.add(c1)
	session.add(c2)
	session.add(c3)
	session.add(u1)
	session.add(u2)
	session.add(u3)
'''
	
