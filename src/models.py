from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime
from geoalchemy2 import Geometry
from sqlalchemy.dialects import postgresql


Base = declarative_base()

'''
jednotlive modely pre serializaciu
'''

class Car(Base):
	__tablename__ = 'car'

	id = Column(Integer, primary_key=True)
	brand = Column(String)
	model = Column(String)
	spz = Column(String)
	color = Column(String)
	#places = Column(Integer)
	fuel = Column(Integer)
	created = Column(DateTime, default=datetime.utcnow)
	updated = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
	is_active = Column(Boolean, default=True)

	#explicitna reprezentacia modelu
	def __repr__(self):
		return '<Car(brand={self.brand!r},model={self.model!r},spz={self.spz!r}, \
			color={self.color!r},fuel={self.fuel!r})>'.format(self=self)


class User(Base):
	__tablename__ = 'user'

	id = Column(Integer, primary_key=True)
	name = Column(String)
	city = Column(String)
	phone = Column(String)
	car_id = Column(Integer, ForeignKey('car.id'))
	created = Column(DateTime, default=datetime.utcnow)
	updated = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
	is_active = Column(Boolean, default=True)
	cdsid = Column(String)
	password = Column(String, nullable=False)
	email = Column(String, unique=True)
	gps = Column(Geometry('POINT'))
	car = relationship('Car', backref='user')
	#ride_user = relationship('RideUsers', backref='ride_user')
	# zmena v realationship secondary, aby sa nemusela pouzivat RideUserSchema a generovat v JSON User: {}
	ride_user = relationship('Ride', backref='ride_user', secondary='ride_with_users')

	#explicitna reprezentacia modelu
	def __repr__(self):
		return '<User(name={self.name!r},city={self.city!r},phone={self.phone!r}, \
			car_id={self.car_id}, password={self.password}, email={self.email})>'.format(self=self)

class Ride(Base):
	__tablename__ = 'ride'

	id = Column(Integer, primary_key=True)
	from_ = Column(String)
	to = Column(String)
	start_date = Column(DateTime)
	end_date = Column(DateTime)
	note = Column(String)
	places = Column(Integer)
	car_id = Column(Integer, ForeignKey('car.id'))
	user_id = Column(Integer, ForeignKey('user.id'))
	gps = Column(Geometry('POINT'))
	gps_dest = Column(Geometry('POINT'))
	is_active = Column(Boolean, default=True)
	route = Column(Geometry('LINESTRING'))
	created = Column(DateTime, default=datetime.utcnow)
	updated = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
	#car = relationship('Car', backref='ride')
	user = relationship('User',backref='ride')
	ride_with_user = relationship('RideUsers', backref='ride')	


	def __repr__(self):
		return '<Ride(from_={self.from_!r},to={self.to!r},start_date={self.start_date!r}, \
			end_date={self.end_date!r}, note={self.note!r}, places={self.places!r},\
			car_id={self.car_id!r}, user_id={self.user_id!r}, gps={self.gps!r}, \
			gps_dest={self.gps_dest!r}, rwu={self.ride_user!r})>'.format(self=self)


# pomocna tabulka pre many-to-many vztah User-Ride
class RideUsers(Base):
	__tablename__ = 'ride_with_users'

	user_id = Column(Integer, ForeignKey('user.id'))
	ride_id = Column(Integer, ForeignKey('ride.id'))
	id = Column(Integer, primary_key=True)
	created = Column(DateTime, default=datetime.utcnow)

	def __repr__(self):
		return '<RideUsers(user_id={self.user_id!r}, ride_id={self.ride_id!r})>'.format(self=self)


class Notification(Base):
	__tablename__ = 'notifications'

	id = Column(Integer, primary_key=True)
	push_id = Column(String)
	os_type = Column(String)
	sandbox = Column(Boolean, default=True)
	user_id = Column(Integer, ForeignKey('user.id'))
	created = Column(DateTime, default=datetime.utcnow)

	def __repr__(self):
		return '<Notification(push_id={self.push_id!r}, os_type={self.os_type!r}, sandbox={self.sandbox!r}, \
			user_id={self.user_id!r})>'.format(self=self)

class Employee(Base):
	__tablename__ = 'employee'

	id = Column(Integer, primary_key=True)
	cdsid = Column(String, unique=True)

