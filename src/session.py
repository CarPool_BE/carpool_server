from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base

import db_build


def db_connect(user, password, db, host='carpool.ctahxu3gwzv8.us-east-2.rds.amazonaws.com', port=5432):
	url = 'postgresql://{}:{}@{}:{}/{}'
	url = url.format(user, password, host, port, db)

	engine = create_engine(url, client_encoding='utf-8', pool_size=20, max_overflow=0)

	#db_build.db_init(engine)
	#Base.metadata.create_all(engine)

	#Session = sessionmaker(bind=engine)
	#session = Session()	

	return engine






