from marshmallow import Schema, fields, post_load, post_dump
import models




'''
Konkretne schemy pre serializaciu/deserializaciu
'''
class CarSchema(Schema):	
	ID = fields.Integer(attribute='id')
	Brand = fields.Str(attribute='brand')
	#Model = fields.Str(attribute='model')
	SPZ = fields.Str(attribute='spz')
	Color = fields.Str(attribute='color')
	#NumberOfPlaces = fields.Integer(attribute='places')
	#FuelType = fields.Integer(attribute='fuel')

	#usporiadane polia, ktore zobrazujem
	class Meta:
		fields = ('ID', 'Brand', 'SPZ', 'Color')
		ordered = True

	#dekorator po load metode vytvorim model
	@post_load
	def make_car(self, data):
		return models.Car(**data)

	@post_dump
	def remove_skip_values(self, data):
		return {
			key: value for key, value in data.items()
			if value != None
		}


class UserSchema(Schema):
	ID = fields.Integer(attribute='id')
	Name = fields.Str(attribute='name')
	City = fields.Str(attribute='city')
	Phone = fields.Str(attribute='phone')
	Car = fields.Nested(CarSchema, attribute='car')
	#Created = fields.DateTime(attribute='created')
	Email = fields.Email(attribute='email')
	Password = fields.Str(attribute='password')
	CDSID = fields.Str(attribute='cdsid')
	FromLon = fields.Number(attribute='lon')
	FromLat = fields.Number(attribute='lat')

	#usporiadane polia, ktore zobrazujem
	class Meta:
		fields = ('ID', 'Name', 'City', 'Phone', 'Car', 'Email', 'Password', 'CDSID', 'FromLon', 'FromLat')
		ordered = True

	@post_dump
	def remove_skip_values(self, data):
		return {
			key: value for key, value in data.items()
			if value != None
		}



class UserSchemaPost(Schema):
	ID = fields.Integer(attribute='id')
	Name = fields.Str(attribute='name')
	City = fields.Str(attribute='city')
	Phone = fields.Str(attribute='phone')
	Car_ID = fields.Integer(attribute='car_id')
	Email = fields.Email(attribute='email')
	Password = fields.Str(attribute='password')
	CDSID = fields.Str(attribute='cdsid')
	GPS = fields.Str(attribute='gps')

	#dekorator po load metode vytvorim model
	@post_load
	def make_user(self, data):
		return models.User(**data)



class RideUserSchema(Schema):
	#User_ID = fields.Integer(attribute='user_id')
	User = fields.Nested(UserSchema, attribute='ride_user', exclude=('Car','Created', 'Password'))
	#User = fields.Str(attribute='Ride.ride_with_user')


class RideSchema(Schema):
	ID = fields.Integer(attribute='Ride.id')
	From = fields.Str(attribute='Ride.from_')
	To = fields.Str(attribute='Ride.to')
	StartDate = fields.DateTime(attribute='Ride.start_date')
	EndDate = fields.DateTime(attribute='Ride.end_date')
	#Time = fields.DateTime(attribute='Ride.date')
	Note = fields.Str(attribute='Ride.note',data_key='Note')
	NumberOfPlaces = fields.Integer(attribute='Ride.places')
	Car_ID = fields.Integer(attribute='Ride.car_id', load_only=True)
	#Car = fields.Nested(CarSchema, attribute='Ride.car' )
	#User_ID = fields.Integer(attribute='Ride.user_id')
	User = fields.Nested(UserSchema, attribute='Ride.user', only=['ID','Name','Phone','Car'])
	#RideWithUsersDetail = fields.Nested(RideUserSchema, attribute='Ride.ride_with_user', many=True)
	# zmena v models v realationship secondary, aby sa nemusela pouzivat RideUserSchema a generovat v JSON User: {}
	RideWithUsersDetail = fields.Nested(UserSchema, attribute='Ride.ride_user', many=True, exclude=('Car','Created', 'Password')) 
	RideWithUsers = fields.Integer(attribute='count')
	Lon = fields.Str(attribute='lon')
	Lat = fields.Str(attribute='lat')
	Driver = fields.Boolean(attribute='is_driver')
	Distance = fields.Number(attribute='distance')
	Walking = fields.Number(attribute='walking')

	#usporiadane polia, ktore zobrazujem
	class Meta:
		fields = ('ID', 'From', 'To', 'StartDate','EndDate', 'Note', 'NumberOfPlaces', \
			'User', 'Car_ID', 'RideWithUsers', 'RideWithUsersDetail', 'Lat', 'Lon',\
			'Driver', 'Distance', 'Walking')
		ordered = True

	@post_dump
	def remove_skip_values(self, data):
		return {
			key: value for key, value in data.items()
			if value != None
		}


class RideSchemaPost(Schema):

	ID = fields.Integer(attribute='id')
	From = fields.Str(attribute='from_')
	To = fields.Str(attribute='to')
	#Date = fields.DateTime(format='%Y-%m-%d %H:%M',attribute='date')
	StartDate = fields.DateTime(attribute='start_date')
	#Date = fields.List(fields.DateTime())
	EndDate = fields.DateTime(attribute='end_date')
	#Time = fields.DateTime(attribute='time')
	Note = fields.Str(attribute='note')
	NumberOfPlaces = fields.Integer(attribute='places')
	Car_ID = fields.Integer(attribute='car_id')
	User_ID = fields.Integer(attribute='user_id')
	GPS = fields.Str(attribute='gps')
	GPS_dest = fields.Str(attribute='gps_dest')
	Route = fields.Str(attribute='route')

	#dekorator po load metode vytvorim model
	@post_load
	def make_ride(self, data):
		return models.Ride(**data)

class TokenSchema(Schema):

	refresh_token = fields.Str(attribute='refresh_token')
	refresh_token_expires_in = fields.Str(attribute='refresh_token_expires_in')
	acces_token = fields.Str(attribute='acces_token')
	acces_token_expires_in = fields.Str(attribute='acces_token_expires_in')


class NotificationSchema(Schema):

	PushID = fields.Str(attribute='push_id')
	OsType = fields.Str(attribute='os_type')
	Sandbox = fields.Boolean(attribute='sandbox')
	UserID = fields.Integer(attribute='user_id')

	@post_load
	def make_notification(self, data):
		return models.Notification(**data)


