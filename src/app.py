import falcon

import resources
import jwt

def create_app():

	# vytvorenie API instancie a nastavenie middleware
	api = falcon.API(middleware=resources.Authorize(['/token']))
	#api = falcon.API()
	# endpointy API
	api.add_route('/cars', resources.Cars())
	api.add_route('/users', resources.Users())
	api.add_route('/cars/{id}', resources.Car())
	api.add_route('/users/{id}', resources.User())
	api.add_route('/rides', resources.Rides())
	api.add_route('/rides/{ride_id}', resources.Ride())
	api.add_route('/me/rides', resources.Rides())
	api.add_route('/me', resources.Me())
	api.add_route('/rides/{ride_id}/passenger/{user_id}', resources.Reservations())
	api.add_route('/token', resources.Token())
	api.add_route('/users/{id}/rides', resources.UserRides())

	return api
