import falcon
import json

import schemas
import models
import session
import jwt
import base64

from datetime import datetime, timedelta, date, timezone
from sqlalchemy import func, or_, literal, and_, cast, Date, case, Numeric, not_
import re
from sqlalchemy.orm import sessionmaker
import logging
from apns2.client import APNsClient
from apns2.payload import Payload
import collections
from pyfcm import FCMNotification

import polyline

#import shapely
import configparser
import io

# Load the configuration file
config = configparser.ConfigParser()
config.read('config.ini')

db_user = config['postgresql']['user']
db_password = config['postgresql']['password']
db_db = config['postgresql']['db']
#SHA-1 - SECRET KEY pre token
#SECRET_KEY = '879d487e8acc5ebdfc43d2506d5793e71c80d3a4'
SECRET_KEY = config['key']['secret_key']
# nastavenie zivotnosti tokenu v sekundach
TOKEN_EXP = 120

# connect do db 
engine = session.db_connect(db_user, db_password, db_db)


client = APNsClient('dev.pem', use_sandbox=False, use_alternative_port=False)
client_sandbox = APNsClient('dev.pem', use_sandbox=True, use_alternative_port=False)
android_client = FCMNotification(api_key='AAAA6xiBK1E:APA91bHrEmGzQpPExQee5bONomWc7-PSKGSSof8NUFIQxMijifYr_UaxKuIwK5IxFlfBYdmX0GQdB-UeMm_cRz3BR8H9dfCgIlxujoTubCz8D4-5mAeh4R0T4JJ4OZ9QTJUOMt_W7ehc')

def db_session(function):
    def wrapper(*args, **kwargs):
        try:    
            Session = sessionmaker(bind=engine)
            session = Session()
            result = function(*args, session, **kwargs)
            return result
        except Exception as e:
            logging.error(e)
            raise e    
        finally:
            session.close()
    return wrapper


# zasielanie push notifikacii
def send_push(mess_type, ride_id, user_id, session):

    #stiahnem si konkretnu jazdu
    ride = session.query(models.Ride).filter(models.Ride.id == ride_id).one_or_none()

    topic = 'eu.teamride.app'
    #Notification = collections.namedtuple('Notification', ['token', 'payload'])
    # notifikacia podla typu spravy, prihlasenie / odhlasenie pasaziera
    if mess_type == 'join' or mess_type == 'leave':
        # stiahnem konkretneho usera pre ziskanie pushID
        passenger = session.query(models.User.name).filter(models.User.id == user_id).one_or_none()

        # sprava pre typ prihlasenia na jazdu
        if mess_type == 'join':
            title = 'Hurá, nový spolujazdec :)'
            alert = '{} sa pridal do Tvojej jazdy na deň {} z lokality {} do cieľa {}.'.\
                format(passenger.name, ride.start_date.strftime('%d.%m.%Y'), ride.from_, ride.to)
        # sprava pre typ odhlasenia z jazdy
        else:
            title = 'Pozor, {} sa odhlásil'.format(passenger.name)
            alert = '{} sa odhlásil z Tvojej jazdy zo dňa {} z lokality {} do cieľa {}.'.\
                format(passenger.name, ride.start_date.strftime('%d.%m.%Y'), ride.from_, ride.to)
        print(alert)
        data_message = { 
            'Name': passenger.name,
            'Date': ride.start_date.strftime('%d.%m.%Y'),
            'From': ride.from_,
            'To': ride.to
        }
        payload = Payload(alert=alert, sound="default", badge=1)
        # stiahnem si pushID usera, moze byt aj viacero 
        push = session.query(models.Notification).filter(models.Notification.user_id == ride.user_id).all()
        #not_list = []
        for push_id in push:
            #not_list.append(Notification(payload=payload, token=push_id.push_id))
            try:
                if push_id.os_type == 'iOS' and push_id.sandbox is True:
                    client_sandbox.send_notification(push_id.push_id, payload, topic)
                elif push_id.os_type == 'iOS' and push_id.sandbox is False:
                    client.send_notification(push_id.push_id, payload, topic)
                elif push_id.os_type == 'Android':
                    android_client.notify_single_device(registration_id=push_id.push_id, message_title=title, message_body=alert, data_message=data_message)
            except Exception as e:
                logging.error(e)
            finally:
                pass

    # ak je sprava typu zrusenie jazdy od sofera
    elif mess_type == 'cancel':
        title = 'Je nám ľúto :('
        alert ='{} zrušil jazdu zo dňa {} z lokality {} do cieľa {}, v ktorej bolo rezervované Tvoje miesto. Nezúfaj a nájdi si náhradu ešte dnes.'.\
            format(ride.user.name, ride.start_date.strftime('%d.%m.%Y'), ride.from_, ride.to)
        print(alert)
        data_message = { 
            'Name': ride.user.name,
            'Date': ride.start_date.strftime('%d.%m.%Y'),
            'From': ride.from_,
            'To': ride.to
        }
        payload = Payload(alert=alert, sound="default", badge=1)
        # prejdem vsetkych prihlasenych, ktorych budem notifikovat
        #not_list = []
        for user in ride.ride_with_user: 
            push = session.query(models.Notification).filter(models.Notification.user_id == user.user_id).all()
            for push_id in push:
                #not_list.append(Notification(payload=payload, token=push_id.push_id))
                try:
                    if push_id.os_type == 'iOS' and push_id.sandbox is True:
                        client_sandbox.send_notification(push_id.push_id, payload, topic)
                    elif push_id.os_type == 'iOS' and push_id.sandbox is False:
                        client.send_notification(push_id.push_id, payload, topic)
                    elif push_id.os_type == 'Android':
                        android_client.notify_single_device(registration_id=push_id.push_id, message_title=title, message_body=alert, data_message=data_message)
                except Exception as e:
                    logging.error(e)
                finally:
                    pass

    #print(not_list)
    #client.send_notification_batch(notifications=not_list, topic=topic)




'''
trieda pre model Car a endpoint /cars 
existujuce metody GET a POST
'''
class Cars():

    #def __init__(self):
    @db_session
    def on_get(self, req, resp, session):

        # vytvorenie schemy pre serializaciu, many = viacero zaznamov
        schema = schemas.CarSchema(many=True)
        db_result = session.query(models.Car).filter(models.Car.is_active).order_by(models.Car.id).all()
        # serialzacia query 
        result = schema.dump(db_result)
        cars = {'Cars': result.data}

        # nastavenie navratovej hodnoty a vypis JSON do body
        resp.status = falcon.HTTP_200
        resp.media = cars

    @db_session
    def on_post(self, req, resp, session):

        user_id = req.context['user_id']

        #vytvorenie schemy pre deserializaciu
        schema = schemas.CarSchema()
        # vyskusam spracovat vstup, inak vyhodim chybu
        try:
            data = json.load(req.stream)
        except:
            raise falcon.HTTPError(falcon.HTTP_400)
        # vyskusam desiarizovat JSON do modelu, inak vyhodim chybu
        try:
            data_load = schema.load(data)
        except:
            raise falcon.HTTPError(falcon.HTTP_422)
        # ulozim si len cast data, error ma nezaujima
        result = data_load.data
        # skontrolujem ci dana SPZ uz existuje v db, inak vratim chybu
        # ak neexistuje vytvorim nove auto
        #car = session.query(models.Car).filter(models.Car.spz == result.spz).first()
        car = session.query(models.Car).join(models.User).filter(models.Car.spz == result.spz, models.User.id == user_id).one_or_none()
        user = session.query(models.User).filter(models.User.id == user_id).one_or_none()
        if car is None:
            session.add(result)
            session.commit()

            #nastavenie usera pre vytvorene auto
            car = session.query(models.Car).filter(models.Car.spz == result.spz, models.Car.color == result.color).first()
            user.car_id = car.id
            session.commit()

            resp.status = falcon.HTTP_201
            resp.media = 'Car created with SPZ: {}'.format(result.spz)
        else:
            resp.status = falcon.HTTP_409
            resp.media = 'Car existing'


'''
trieda pre model Car a endpoint cars/{id}
existujuce metody GET, DELETE, PATCH (stacia mi len hodnoty, ktore menim)
'''
class Car():

   # def __init__(self):
    def on_get(self, req, resp, id):

        resource_get('Car', id, resp)

    def on_delete(self, req, resp, id):

        # nastavim si user_id z kontextu pre autorizacnu kontrolu vymazania
        #user_id = req.context['user_id']
        resource_delete('Car', id, req, resp)

    @db_session
    def on_patch(self, req, resp, session, id):
        #user_id = req.context['user_id']
        try:
            db_result = session.query(models.Car).get(int(id))
        except ValueError:
            db_result = None

        if db_result is not None:
        #    db_user = session.query(models.User).filter(models.User.id == user_id).one_or_none()
        #    if db_user.car_id != db_result.id:
        #        raise falcon.HTTPError(falcon.HTTP_401)
            try:
                data = json.load(req.stream)
            except:
                raise falcon.HTTPError(falcon.HTTP_400)
            try:
                schema = schemas.CarSchema()
                data_load = schema.load(data)
                if data_load.errors:
                    raise falcon.HTTPError(falcon.HTTP_422)
            except:
                raise falcon.HTTPError(falcon.HTTP_422)
            result = data_load.data
            for attr, val in list(vars(result).items())[1:]:
                setattr(db_result, attr, val)
            try:
                session.commit()
            except:
                raise falcon.HTTPError(falcon.HTTP_400)
        else:
            resp.status = falcon.HTTP_404
            resp.media = 'Car not found for id: {}'.format(id)
        #session.close()


'''
trieda pre model User a endpoint /users 
existujuce metody GET a POST
'''
class Users():

    #def __init__(self):
    @db_session
    def on_get(self, req, resp, session):

        schema = schemas.UserSchema(many=True)
        db_result = session.query(models.User).filter(models.User.is_active).order_by(models.User.id).all()
        result = schema.dump(db_result)
        users = {'Users': result.data}

        resp.status = falcon.HTTP_200
        resp.media = users
        #session.close()

    @db_session
    def on_post(self, req, resp, session):

        schema = schemas.UserSchemaPost()
        try:
            data = json.load(req.stream)
        except:
            raise falcon.HTTPError(falcon.HTTP_400)
        try:
            data = format_gps(data)
            data_load = schema.load(data)
        except:
            raise falcon.HTTPError(falcon.HTTP_422)
        result = data_load.data
        #cdsid = session.query(models.Employee).filter(models.Employee.cdsid == result.cdsid).one_or_none()
        #if cdsid is None:
        #    resp.media = 'ID not found'
        #    raise falcon.HTTPError(falcon.HTTP_404)
        # kontrola na zaklade emailu a hesla
        user = session.query(models.User).filter(models.User.email == result.email).count()
        if user == 0:
            result.password = func.crypt(result.password, func.gen_salt('bf'))
            session.add(result)
            session.commit()
            resp.status = falcon.HTTP_201
            resp.media = 'User created with name: {}'.format(result.name)
        else:
            resp.status = falcon.HTTP_409
            resp.media = 'User existing'
        #session.close()



'''
trieda pre model User a endpoint /users/{id} 
existujuce metody GET, DELETE a PATCH
'''
class User():

    #def __init__():

    def on_get(self, req, resp, id):

        resource_get('User', id, resp)   


    def on_delete(self, req, resp, id):

        resource_delete('User', id, req, resp)

        #user_id = req.context['user_id']

    @db_session
    def on_patch(self, req, resp, session, id):

        user_id = req.context['user_id']
        try:
            db_result = session.query(models.User).get(int(id))
        except ValueError:
            db_result = None

        if db_result is not None:
            if user_id != db_result.id:
                raise falcon.HTTPError(falcon.HTTP_401)
            try:
                data = json.load(req.stream)
                data = format_gps(data)
            except:
                raise falcon.HTTPError(falcon.HTTP_400)
            try:
                schema = schemas.UserSchemaPost()
                data_load = schema.load(data)
                if data_load.errors:
                    raise faclon.HTTPError(falcon.HTTP_422)
            except:
                raise falcon.HTTPError(falcon.HTTP_422)
            result = data_load.data
            for attr, val in list(vars(result).items())[1:]:
                if(attr == 'password'):
                    val = func.crypt(val, func.gen_salt('bf'))
                setattr(db_result, attr, val)
            try:
                session.commit()
            except:
                raise falcon.HTTPError(falcon.HTTP_400)
        else:
            resp.status = falcon.HTTP_404
            resp.media = 'User not found for id: {}'.format(id)
        #session.close()


'''
trieda pre model User a endpoint /me - sluzi ako moj profil 
existujuce metody GET a PATCH
'''
class Me():

    def on_get(self, req, resp):

        if req.path == '/me':
            #id = req.get_header('id', default='1')
            user_id = req.context['user_id']

        resource_get('Me', user_id, resp)


    def on_patch(self, req, resp):

        if req.path == '/me':
            #id = req.get_header('id', default='1')
            user_id = req.context['user_id']

        resource_patch('User', user_id, req, resp)



'''
trieda pre model Ride a endpoint /rides a /me/rides - sluzi pre zobrazenie jazd
existujuce metody GET a POST
'''
class Rides():

    @db_session
    def on_get(self, req, resp, session):

        # nastavenie schemy, kde nezobrazujem info o aute a detaily o prihlasenych
        schema = schemas.RideSchema(many=True, exclude=('User.Car','RideWithUsersDetail'))

        # ziskam user_id z kontextu zaslaneho tokenu
        user_id = req.context['user_id']

        date_param = False
        date_filter = date.today()
        if 'date' in req.params:
            date_param = True
            date_filter = datetime.strptime(req.params['date'].split('T')[0], '%Y-%m-%d')
        else:
            raise falcon.HTTPError(falcon.HTTP_400, 'Missing query param date')

        # endpoint pre moje jazdy 
        if req.path == '/me/rides':
            #header = req.get_header('id', default='1')
            sub = session.query(models.RideUsers).outerjoin(models.Ride).group_by(models.RideUsers).subquery('sub')
            rides_passenger = session.query(models.Ride,\
                func.ST_X(func.ST_AsText(models.Ride.gps)).label('lon'),\
                func.ST_Y(func.ST_AsText(models.Ride.gps)).label('lat'),\
                func.count(models.RideUsers.user_id).label('count'),\
                case([(models.Ride.user_id == int(user_id),literal(True))],\
                else_ = literal(False)).label('is_driver')
                ).filter(models.Ride.is_active, and_(user_id == sub.c.user_id, models.Ride.id == sub.c.ride_id),\
                (cast(models.Ride.start_date, Date) == date_filter)).outerjoin(models.RideUsers).group_by(models.Ride.id)
            rides_driver = session.query(models.Ride,\
                func.ST_X(func.ST_AsText(models.Ride.gps)).label('lon'),\
                func.ST_Y(func.ST_AsText(models.Ride.gps)).label('lat'),\
                func.count(models.RideUsers.user_id).label('count'),\
                case([(models.Ride.user_id == int(user_id),literal(True))],\
                else_ = literal(False)).label('is_driver')
                ).filter(models.Ride.is_active, models.Ride.user_id == int(user_id),\
                (cast(models.Ride.start_date, Date) == date_filter)).outerjoin(models.RideUsers).group_by(models.Ride.id)

            rides = rides_driver.union(rides_passenger).order_by(models.Ride.start_date).all()
            
            
                # + obmedzenie .filter(cast(models.Ride.start_date, Date) >= date.today())
            #print(rides)
        # endpoint pre /rides
        else:
            # nastavim si lokalitu prace 
            destination = 'Jaguar Landrover, Nitra'
            # parameter pre hladanie Z prace (True) alebo DO prace (False)
            from_param = False

            if 'from' in req.params:
                param = req.params['from']
                if param == 'Jaguar Landrover, Nitra':
                    from_param = True
            else:
                destination = '%'

            # zadanie gps v tvare Lon,Lat a vytvorenie bodu pre select
            if 'gps_from' in req.params:
                lon, lat = req.params['gps_from'].split(',')
                point_from = func.ST_MakePoint(lon, lat)
            else: 
                raise falcon.HTTPError(falcon.HTTP_400, 'Missing query param gps from')
            if 'gps_to' in req.params:
                lon, lat = req.params['gps_to'].split(',')
                point_to = func.ST_MakePoint(lon, lat)
            else:
                raise falcon.HTTPError(falcon.HTTP_400, 'Missing query param gps to')

            # log pre zaznamenanie vyhladavania
            if 'to' in req.params:
                print('User: {}, From: {}, To: {}, DateTime: {}, DateTimeSearch: {}'.format(user_id, req.params['from'], req.params['to'], req.params['date'],datetime.utcnow()))
            else:
                print('User: {}, From: {}, To: {}, DateTime: {}, DateTimeSearch: {}'.format(user_id, req.params['from'], req.params['gps_to'], req.params['date'],datetime.utcnow()))

            # zobrazim si ride, lon, lat, pocet prihasenych, vzdialenost medzi bodmi From
            # vzdialenost medzi bodmi To
            # zoradene podla casu zacatia jazdy, kde zobrazim len existujuce jazdy
            # podla zadaneho parametru from zobrazim jazdy do alebo z prace, ak nie je zadane tak hladam vsetky jazdy
            # podla zadaneho datumu zobrazim jazdy na konkretny den alebo 
            # vsetky jazdy od dnesneho dna 
            # obmedzene na vzdialenost 1200m, ak hladam do JLR tak vzdialenost gps a point_from inak gps_dest a point_to
            # obmedzenie, ze nevyhladavam plne jazdy a jazdy, ktore som vytvoril ja sam
            try:
                sub = session.query(models.RideUsers).filter(user_id == models.RideUsers.user_id).group_by(models.RideUsers).subquery('sub')
                my_rides = session.query(models.RideUsers.ride_id).filter(user_id == models.RideUsers.user_id).group_by(models.RideUsers).all()

                ride_list = []
                for one_ride in my_rides:
                    ride_list.append(one_ride.ride_id)
                #print(ride_list)
                #hladanie z JLR
                if from_param:
                    rides = session.query(models.Ride,\
                        func.count(models.RideUsers.user_id).label('count'),
                        func.ST_X(func.ST_AsText(models.Ride.gps)).label('lon'),\
                        func.ST_Y(func.ST_AsText(models.Ride.gps)).label('lat')\
                        ,cast((func.ST_Distance_Sphere(models.Ride.gps_dest, point_to)/1000.0),Numeric(4,2)).label('distance')
                        ,cast((func.ST_Distance_Sphere(models.Ride.gps_dest, point_to)*0.75/60),Numeric(3,0)).label('walking')
                        ).outerjoin(models.RideUsers).group_by(models.Ride.id).\
                        order_by(models.Ride.start_date).\
                        filter(models.Ride.is_active, models.Ride.from_.like(destination), func.ST_Distance_Sphere(models.Ride.gps_dest, point_to) <= 2000.0,\
                        models.Ride.id.notin_(ride_list), \
                        case([(date_param == literal(True), cast(models.Ride.start_date, Date) == date_filter)],\
                        else_ = cast(models.Ride.start_date, Date) >= date_filter), models.Ride.user_id != int(user_id)). \
                        having(func.count(models.RideUsers.user_id) < models.Ride.places).all()
                        #or_(models.RideUsers.user_id == None, and_(user_id != sub.c.user_id, models.Ride.id == sub.c.ride_id)),\
                else:
                    rides = session.query(models.Ride,\
                        func.count(models.RideUsers.user_id).label('count'),\
                        func.ST_X(func.ST_AsText(models.Ride.gps)).label('lon'),\
                        func.ST_Y(func.ST_AsText(models.Ride.gps)).label('lat')\
                        ,cast((func.ST_Distance_Sphere(models.Ride.gps, point_from)/1000.0),Numeric(4,2)).label('distance')
                        ,cast((func.ST_Distance_Sphere(models.Ride.gps, point_from)*0.75/60),Numeric(3,0)).label('walking')
                        ).outerjoin(models.RideUsers).group_by(models.Ride.id).\
                        order_by(models.Ride.start_date).\
                        filter(models.Ride.is_active, models.Ride.to.like(destination), func.ST_Distance_Sphere(models.Ride.gps, point_from) <= 2000.0,\
                        models.Ride.id.notin_(ride_list), \
                        case([(date_param == literal(True), cast(models.Ride.start_date, Date) == date_filter)],\
                        else_ = cast(models.Ride.start_date, Date) >= date_filter), models.Ride.user_id != int(user_id)).\
                        having(func.count(models.RideUsers.user_id) < models.Ride.places).all()

            except ValueError:
                raise falcon.HTTPError(falcon.HTTP_400)
            # prerobit bez .all() a uzavriet pred and_, potom nastavit if a tam vybrat co chcem 
            # vybrat z db pomocou all()

        if len(rides) == 0:
            resp.status = falcon.HTTP_404
            resp.media = 'Rides not found!'

        else:
            result = schema.dump(rides)
            result = result.data
            test = {'Rides': result} 
            #print(test)


            resp.status = falcon.HTTP_200
            resp.media = test

        #session.close()


    # MULTIPOST - spracujem viacero datumov jednym requestom a vytvorim 
    # jednosmerne jazdy per den
    @db_session
    def on_post(self, req, resp, session):

        schema = schemas.RideSchemaPost()
        try:
            data = json.load(req.stream)
        except:
            raise falcon.HTTPError(falcon.HTTP_400)
        try:
            data = format_gps(data)
            data = format_route(data)
            #print(data['Route'])

            t = data['Time'].split('T')[1]
            t_end = data['TimeETA'].split('T')[1]
            for row in data['Date']:
                d = row.split('T')[0]
                date = d + 'T' + t
                date_end = d + 'T' + t_end
                date = {'StartDate': date}
                date_end = {'EndDate': date_end}
                data.update(date)
                data.update(date_end)
                data_load = schema.load(data)
                #print(data_load.data)
                session.add(data_load.data)

            if data['IsReturn']:
                t = data['ReturnTime'].split('T')[1]
                t_end = data['ReturnTimeETA'].split('T')[1]
                swap_FT = {'From': data['To'],'To': data['From']}
                places = {'NumberOfPlaces': data['ReturnNumberOfPlaces']}
                note = {'Note': data['ReturnNote']}
                gps = {'GPS': data['GPS_dest'], 'GPS_dest': data['GPS']}
                data.update(swap_FT)
                data.update(places)
                data.update(note)
                data.update(gps)
                for row in data['Date']:
                    d = row.split('T')[0]
                    date = d + 'T' + t
                    date_end = d + 'T' + t_end
                    date = {'StartDate': date}
                    date_end = {'EndDate': date_end}
                    data.update(date)
                    data.update(date_end)
                    data_load = schema.load(data)
                    #print(data_load.data)
                    session.add(data_load.data)


        except:
            raise falcon.HTTPError(falcon.HTTP_422)
        session.commit()
        resp.status = falcon.HTTP_201
        resp.media = 'Rides created'
        #session.close()


'''
endpoint rides/{id} - sluzi pre detail konkretnej jazdy 
'''
class Ride():


    def on_get(self, req, resp, ride_id):

        resource_get('Ride', ride_id, resp)


    #def on_patch(self, req, resp, ride_id):

    #    resource_patch('Ride', ride_id, req, resp)


    def on_delete(self, req, resp, ride_id):

        resource_delete('Ride', ride_id, req, resp)



'''
endpoint /users/{id}/rides - sluzi pre zobrazenie vsetkych jazd konkretneho sofera
'''
class UserRides():

    @db_session
    def on_get(self, req, resp, session, id):

        # ziskam user_id z kontextu zaslaneho tokenu
        user_id = req.context['user_id']

        # nastavenie schemy, kde nezobrazujem info o aute a detaily o prihlasenych
        schema = schemas.RideSchema(many=True, exclude=('RideWithUsersDetail',))

        my_rides = session.query(models.RideUsers.ride_id).filter(user_id == models.RideUsers.user_id).group_by(models.RideUsers).all()

        ride_list = []
        for one_ride in my_rides:
            ride_list.append(one_ride.ride_id)

        rides = session.query(models.Ride,\
            func.ST_X(func.ST_AsText(models.Ride.gps)).label('lon'),\
            func.ST_Y(func.ST_AsText(models.Ride.gps)).label('lat'),\
            func.count(models.RideUsers.user_id).label('count'),\
            ).outerjoin(models.Ride.ride_with_user).group_by(models.Ride.id).\
            filter(models.Ride.is_active, models.Ride.user_id == int(id), models.Ride.id.notin_(ride_list), \
            cast(models.Ride.start_date, Date) >= date.today()).\
            having(func.count(models.RideUsers.user_id) < models.Ride.places). \
            order_by(models.Ride.start_date).all()
        # + obmedzenie .filter(cast(models.Ride.start_date, Date) >= date.today())

        if len(rides) == 0:
            resp.status = falcon.HTTP_404
            resp.media = 'Rides not found!'

        else:
            result = schema.dump(rides)
            result = result.data
            rides = {'Rides': result}     

            resp.status = falcon.HTTP_200
            resp.media = rides

        #session.close()



'''
endpoint /rides/{id}/passenger/{user_id}
'''
class Reservations():

    #@db_session
    def on_put(self, req, resp, ride_id, user_id):
        Session = sessionmaker(bind=engine)
        session = Session()
        # vyskusam ziskat existujucu rezervaciu
        try: 
            reservation = session.query(models.RideUsers).filter(\
            and_(models.RideUsers.ride_id == ride_id,\
            models.RideUsers.user_id == user_id)).first()
        except ValueError:
            raise falcon.HTTPError(falcon.HTTP_422)
        # stiahnem si pocet miest v jazde a pocet zarezervovanych
        try:
            ride = session.query(models.Ride).filter(models.Ride.id == ride_id).one_or_none()
            n_reservations = session.query(models.RideUsers).filter(models.RideUsers.ride_id == ride_id).count()
            passenger = session.query(models.User.name).filter(models.User.id == user_id).one_or_none()
        except ValueError:
            raise falcon.HTTPError(falcon.HTTP_422)
        # ak som zaplnil pocet miest a niekto sa pokusi o rezervaciu hadzem chybu
        #print(ride.places)
        if ride.places <= n_reservations:
            resp.status = falcon.HTTP_409
            resp.media = 'Ride is full'
        # inak pokracujem a kontrolujem ci uz mam na konkretnu jazdu rezervaciu vytvorenu
        else:
            if reservation is None:
                result = models.RideUsers(ride_id = ride_id, user_id = user_id)
                session.add(result)
                session.commit()
                send_push('join', ride_id, user_id, session)
                resp.status = falcon.HTTP_201
                resp.media = 'Reservation created'
                #push notifikacia o rezervovani jazdy
            else:
                resp.status = falcon.HTTP_409
                resp.media = 'Reservation already exist'
        session.close()


    #@db_session
    def on_delete(self, req, resp, ride_id, user_id):
        Session = sessionmaker(bind=engine)
        session = Session()
        # pokusim sa nacitat si rezervaciu pre konkretnu jazdu a usera
        try:
            reservation = session.query(models.RideUsers).filter(\
                and_(models.RideUsers.ride_id == ride_id,\
                models.RideUsers.user_id == user_id)).first()
            passenger = session.query(models.User.name).filter(models.User.id == user_id).one_or_none()
            ride = session.query(models.Ride).filter(models.Ride.id == ride_id).one_or_none()
        except ValueError:
            reservation = None

        # ak dana rezervacia existuje vymazem a vypisem OK, inak hodim chybu
        if reservation is not None:
            session.delete(reservation)
            session.commit()
            send_push('leave', ride_id, user_id, session)
            resp.status = falcon.HTTP_200
            resp.media = 'Reservation cancelled'
        else:
            resp.status = falcon.HTTP_404
            resp.media = 'Reservation doesnt exist'

        session.close()


'''
trieda pre autentifikaciu pouzivatela 
'''
class Authorize():

    # nastavenie povolenych endpointov
    def __init__(self, allowed_paths=[]):
        self._allowed_paths = allowed_paths

    # processing pri requeste
    def process_request(self, req, resp):

        if req.path == '/users' and req.method == 'POST':
            return

        if req.path == '/token' and (req.method == 'GET' or req.method =='POST'):
            return


        # ak je endpoint v povolenych paths - nekontrolujem nic
        #if req.path in self._allowed_paths:
        #    return

        if req.auth:
            try:
                bearer, token = req.auth.split()
            except ValueError:
                raise falcon.HTTPError(falcon.HTTP_401)

        # ak nie je v hlavicke authorize alebo token nie je typu Bearer vratim chybu
        if not req.auth or req.auth.split()[0] != 'Bearer':
            raise falcon.HTTPError(falcon.HTTP_401)
        # overujem token - ak zlyha vraciam chybu
        try:
            jwt_data = jwt.decode(req.auth.split()[1], SECRET_KEY, algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            raise falcon.HTTPError(falcon.HTTP_403)
        except jwt.InvalidTokenError:
            raise falcon.HTTPError(falcon.HTTP_400)
        # overenie preslo bez chyby, ulozim do kontextu requestu user_id
        # ktore mozeme pouzit v resource
        else:
            req.context['user_id'] = int(jwt_data['sub'])

'''      
trieda pre ziskanie JWT tokenu a autentifikacia uzivatela
'''
class Token():

    @db_session
    def on_get(self, req, resp, session):

        if 'refresh_token' in req.params:
            refresh_token = req.params['refresh_token']
        else:
            raise falcon.HTTPError(falcon.HTTP_400)

        try:
            refresh_data = jwt.decode(refresh_token, SECRET_KEY, algorithms=['HS512'])
        except jwt.ExpiredSignatureError:
            raise falcon.HTTPError(falcon.HTTP_403)
        except jwt.InvalidTokenError:
            raise falcon.HTTPError(falcon.HTTP_400)

        # dorobit kontrolu na refresh token s db - vytvorit novu tabulku

        #print(refresh_data['sub'])
        #print(data['refresh_token'])

        jwt_data = {
            'exp': datetime.utcnow() + timedelta(minutes=TOKEN_EXP),
            'sub': str(refresh_data['sub'])
        }

        jwt_token = jwt.encode(jwt_data, SECRET_KEY, algorithm='HS256')
        expires = datetime.utcnow() + timedelta(minutes=TOKEN_EXP)
        expires_acces = expires.strftime("%Y-%m-%dT%H:%M:%S+00:00")

        resp.media = {
            'acces_token': jwt_token.decode(),
            'acces_token_expires_in': expires_acces
        }


    
    @db_session
    def on_post(self, req, resp, session):
        # pre vystvenie tokenu potrebujeme autentifikovat pouzivatela
        # menom a heslom, takze ak nemame Authorization hlavicku, vratime chybu
        if not req.auth:
            raise falcon.HTTPError(falcon.HTTP_400, 'Missing Authorization')

        auth_type, auth_token = req.auth.split(' ')

        #message = "om|eH_tkmB@KFEnAHF?Fy@Bq@?qA?_@PuAvAyH^uA\\}@jA}BNYp@oAlAoBXWFGFBJALS?]KOSAi@s@MIaAeBcAmBGGDK@KCQEGIAID_@u@mBiDAYs@}@yAyCMg@EGK]SOKAQBUTGJCf@D\\DHKTAh@MtBUhBc@dBuA|DgAzCgJ~V{BxF{BvEgCjEiA~Ay@hAgHrIo@z@gAhBEHa@b@u@rAs@`AiAxAqCtCw@n@gElCW\\oCnCc@h@yBfCGPw@z@sCbDo@z@_@l@uEbKkExL}@xB]n@OXIPOt@KPKt@UfA{HzTiBfGgDlLSf@w@lCUr@_@x@w@tAEH]|AYzAGNS@OJONKVEXA\\@\\HXEVITi@`@_@\\W`@KVs@`C]z@o@x@_BxAEDWPsA~@{C|Bc@`@s@n@wB`C{BrC_ErFAH?RFXLf@Nl@Lh@Jb@Lh@Ld@Nn@Lh@J`@Jf@Lh@Lf@Nj@Jh@Ld@Lf@LP"
        #media = {'Route': message}
        #p = format_route(media)
        #resp.media = {'test':p['Route']}
        #return
        

        # ocakavame Basic autentifikaciu, inac vratime chybu
        if not auth_type == 'Basic':
            raise falcon.HTTPError(falcon.HTTP_400)

        # z hlavicky ziskame login, heslo a skusime podla nich najst pouzivatela
        login, password = base64.b64decode(auth_token).decode().split(':')
        user = session.query(models.User).filter(models.User.email == login, models.User.password == func.crypt(password, models.User.password)).one_or_none()


        if user is None:
            raise falcon.HTTPError(falcon.HTTP_403)

        schema = schemas.NotificationSchema()
        
        try:
            data = json.load(req.stream)
            #print(data)
            data_load = schema.load(data)
            notify = data_load.data
            notify.user_id = user.id
            push = session.query(models.Notification).filter(models.Notification.push_id == notify.push_id, models.Notification.user_id == notify.user_id).one_or_none()
            if push is None:
                print(notify)
                session.add(notify)
                session.commit()
        except:
            raise falcon.HTTPError(falcon.HTTP_400, 'Bad JSON')

        # pripravenie dat na token
        jwt_data = {
            'exp': datetime.utcnow() + timedelta(minutes=TOKEN_EXP),
            'sub': str(user.id)
        }

        refresh_data = {
            'exp': datetime.utcnow() + timedelta(days=TOKEN_EXP),
            'sub': str(user.id)
        }

        # vygenerovanie tokenu
        jwt_token = jwt.encode(jwt_data, SECRET_KEY, algorithm='HS256')
        refresh_token = jwt.encode(refresh_data, SECRET_KEY, algorithm='HS512')
        expires_refresh = datetime.now(timezone.utc) + timedelta(days=TOKEN_EXP)
        expires_acces = datetime.now(timezone.utc) + timedelta(minutes=TOKEN_EXP)
        expires_refresh = expires_refresh.strftime("%Y-%m-%dT%H:%M:%S+00:00")
        expires_acces = expires_acces.strftime("%Y-%m-%dT%H:%M:%S+00:00")
        #expires_acces = datetime.strptime(expires_acces,"%Y-%m-%dT%H:%M:%S%Z")
        #expires_refresh = datetime.strptime(expires_refresh,"%Y-%m-%dT%H:%M:%S%Z")

        JSON_data = {
            'acces_token': jwt_token.decode(),
            'acces_token_expires_in': expires_acces,
            'refresh_token': refresh_token.decode(),
            'refresh_token_expires_in': expires_refresh
        }

        schema = schemas.TokenSchema()
        JSON_data = schema.dump(JSON_data)
        # vratime token podla two-legged OAuth 2.0
        resp.media = JSON_data.data


    @db_session
    def on_delete(self, req, resp, session):

        user_id = req.context['user_id']

        schema = schemas.NotificationSchema()

        if 'push_id' in req.params:
            push_id = req.params['push_id']
        else:
            raise falcon.HTTPError(falcon.HTTP_400, 'Missing query param push_id')

        try:
            #data = json.load(req.stream)
            #print(data)
            #data_load = schema.load(data)
            push = session.query(models.Notification).filter(models.Notification.push_id == push_id, models.Notification.user_id == user_id).one_or_none()
            if push:
                session.delete(push)
                session.commit()
                resp.status = falcon.HTTP_200
                resp.media = 'Disconnected'
            else:
                resp.status = falcon.HTTP_404
                resp.media = 'PushID or UserID not found'
        except:
            raise falcon.HTTPError(falcon.HTTP_400, 'Query fault')

        



'''
 metoda pre rozdelenie typu Datetime na Date a Time pre serializaciu do JSON vystupu
'''
def split_date(result):
    for row in result:
            if row['Date'] is not None:
                d,t = row['Date'].split('T')
                d = {'Date': d}
                t = {'Time': '{:.5}'.format(t)}
                row.update(d)
                row.update(t)

    return result
'''
 metoda pre spracovanie Lat,Lon na typ Geom pre ulozenie do db 
 spracovanie pre adresy From a To
'''
def format_gps(data):
    if data.get('FromLat') and data.get('FromLon') is not None:
        Lat = data['FromLat']
        Lon = data['FromLon']
        geom = 'POINT({} {})'.format(Lon,Lat)
        d  = {'GPS': geom}
        data.update(d)
        del data['FromLat']
        del data['FromLon']
    if data.get('ToLat') and data.get('ToLon') is not None:
        Lat = data['ToLat']
        Lon = data['ToLon']
        geom = 'POINT({} {})'.format(Lon,Lat)
        d = {'GPS_dest': geom}
        data.update(d)
        del data['ToLat']
        del data['ToLon']

    return data

def format_route(data):
    if data.get('Route') is not None:
        route = polyline.decode(data['Route'])
        linestring = 'LINESTRING('
        # ulozenie v tvare Lon, Lat - musi sa otocit, framework z FE zasiela v tvare Lat,Lon
        for point in route:
            linestring += '{} {},'.format(point[1], point[0])
        linestring = linestring[:-1]
        linestring += ')'
        r = {'Route': linestring}
        data.update(r)

    return data


'''
genericka metoda pre GET sluzi pre modely CAR,USER,RIDE
'''
@db_session
def resource_get(name, id, resp, session):

    # podla parametru name nastavim model a shcemu 
    if name is 'Car':
        model = models.Car
        schema = schemas.CarSchema()
    elif name is 'User':
        model = models.User
        schema = schemas.UserSchema(exclude=('Password', 'ID_emp','Created',))
    elif name is 'Me':
        model = models.User
        schema = schemas.UserSchema()
    elif name is 'Ride':
        model = models.Ride
        # nastavim schemu bez poctu prihlasenych pouzivatelov
        schema = schemas.RideSchema(exclude=('RideWithUsers',))

    
    # vyskusam vyhladat detail jazdy/auta/usera
    try:
        if name is 'Ride':      
            db_result = session.query(model,\
                func.ST_X(func.ST_AsText(model.gps)).label('lon'),\
                func.ST_Y(func.ST_AsText(model.gps)).label('lat'),\
                ).filter(model.id == int(id), model.is_active).one_or_none()
        elif name is 'Me':
            db_result = session.query(model).filter(model.id == int(id), model.is_active).one_or_none()
            gps = session.query(func.ST_X(func.ST_AsText(model.gps)).label('lon'),\
                func.ST_Y(func.ST_AsText(model.gps)).label('lat'),\
                ).filter(model.id == int(id), model.is_active).one_or_none()
            db_result.lon = gps[0]
            db_result.lat = gps[1]
        else:
            db_result = session.query(model).filter(model.id == int(id), model.is_active).one_or_none()
    except ValueError:
        db_result = None

    # ak existuje hodim na vystup a nastavim navratovu hodnotu
    if db_result is not None:
        result = schema.dump(db_result)
        resp.status = falcon.HTTP_200
        resp.media = result.data
    else:
        resp.status = falcon.HTTP_404
        resp.media = '{} not found for id: {}'.format(name,id)

    #session.close()


'''
genericka metoda PATCH pre modely CAR, USER, RIDE
'''
@db_session
def resource_patch(name,id,req,resp, session):

    if name is 'Car':
        model = models.Car
        schema = schemas.CarSchema()
    elif name is 'User':
        model = models.User
        schema = schemas.UserSchemaPost()
    elif name is 'Ride':
        model = models.Ride
        schema = schemas.RideSchemaPost()

    try:
        db_result = session.query(model).get(int(id))
    except ValueError:
        db_result = None

    if db_result is not None:
        #    db_user = session.query(models.User).filter(models.User.id == user_id).one_or_none()
        #    if db_user.car_id != db_result.id:
        #        raise falcon.HTTPError(falcon.HTTP_401)
        try:
            data = json.load(req.stream)
            data = format_gps(data)
        except:
            raise falcon.HTTPError(falcon.HTTP_400)
        try:
            data_load = schema.load(data)
            if data_load.errors:
                raise falcon.HTTPError(falcon.HTTP_422)
        except:
            raise falcon.HTTPError(falcon.HTTP_422)
        result = data_load.data
        for attr, val in list(vars(result).items())[1:]:
            # kvoli unique zaznamu v db potrebujem kontrolovat ci sa meni email, je v JSON a bez zmeny, tak pass
            if(attr == 'car'):
                pass
            elif(attr == 'email'):
                if(db_result.email == val):
                    pass
                else:
                    setattr(db_result, attr, val)
            else:
                if(attr == 'password'):
                    val = func.crypt(val, func.gen_salt('bf'))
                setattr(db_result, attr, val)
        try:
            session.commit()
        except:
            raise falcon.HTTPError(falcon.HTTP_400)
        resp.status = falcon.HTTP_200
        resp.media = 'Change succesful'
    else:
        resp.status = falcon.HTTP_404
        resp.media = '{} not found for id: {}'.format(name,id)

    #session.close()



'''
genericka metoda DELETE pre modely CAR, USER, RIDE
'''
@db_session
def resource_delete(name, id, req, resp, session):

    # nastavim si user_id z kontextu pre autorizacnu kontrolu vymazania
    user_id = req.context['user_id']

    # vyskusam ziskat zaznam z db

    if name is 'Car':
        model = models.Car
    elif name is 'User':
        model = models.User
    elif name is 'Ride':
        model = models.Ride


    try:
        db_result = session.query(model).get(int(id))
    except ValueError:
        db_result = None

    # ak zaznam existuje a ja som jeho vlastnikom, vymazem a vratim OK
    if db_result is not None:
        if name is 'User':
            if user_id != db_result.id:
                raise falcon.HTTPError(falcon.HTTP_401)
        elif name is 'Ride':
            if user_id != db_result.user_id:
                raise falcon.HTTPError(falcon.HTTP_401)

        db_result.is_active = False
        session.commit()
        # vytvorenie message pre push
        if name is 'Ride':
            send_push('cancel', db_result.id, None, session)

        resp.status = falcon.HTTP_200
        resp.media = '{} with id: {} deleted'.format(name, id)
    else:
        resp.status = falcon.HTTP_404
        resp.media = '{} not found for id: {}'.format(name, id)
        
    #session.close()



